﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAD_EDI.Models
{
    public class Token
    {
        public string token { get; set; }
        public Int64 expires { get; set; }
        public string user { get; set; }
    }
}

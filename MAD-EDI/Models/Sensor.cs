﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAD_EDI.Models
{
   public class Sensor
    {
        public Int32 ID { get; set; }
        public Int32 NodeID { get; set; }
        public Int32 RoomID { get; set; }
        public DateTime Timestamp { get; set; }
        public String Deviceidentifier { get; set; }
        public String Name { get; set; }
        public String Type { get; set; }
        public String Description { get; set; }
        public double MapLocationX { get; set; }
        public double MapLocationY { get; set; }
        public List<Measurement> measurements { get; set; }
    }
}

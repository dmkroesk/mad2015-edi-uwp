﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using System.Collections.ObjectModel;
using Template10.Mvvm;
//Essential Studio Library
using Syncfusion.UI.Xaml.Charts;
using MAD_EDI.Services;

namespace MAD_EDI.Models.Drawable
{
    public class GraphLineModel : BindableBase
    {

        public Array EmptyPointStyles
        {
            get
            {
                return Enum.GetValues(typeof(EmptyPointStyle));
            }
        }

        public Array EmptyPointValues
        {
            get
            {
                return Enum.GetValues(typeof(EmptyPointValue));
            }
        }

        public Array LegendPosition
        {
            get
            {
                return Enum.GetValues(typeof(LegendPosition));
            }
        }

        public Array ChartDock
        {
            get
            {
                return Enum.GetValues(typeof(ChartDock));
            }
        }

        public Array VerticalAlignment
        {
            get
            {
                return Enum.GetValues(typeof(VerticalAlignment));
            }
        }

        public Array HorizontalAlignment
        {
            get
            {
                return Enum.GetValues(typeof(HorizontalAlignment));
            }
        }

        public Sensor Sensor { get; set; }

        public GraphLineModel(Sensor sensor)
        {
            this.data = new ObservableCollection<Measurement>();
            this.Sensor = sensor;
            var UnusedTask = PopulateGraphData(Sensor);
            /* I got this from the Essential Studio sample, but it's worth noting that
               the used form of platform checking (AnalyticsInfo.VersionInfo.DeviceFamily)
               is actually for debugging purposes and is an unsafe way to check what platform 
               you're running on.
            
            if (Windows.System.Profile.AnalyticsInfo.VersionInfo.DeviceFamily != "Windows.Mobile")
            {
                data.Add(new Measurement() { Year = yr.AddYears(5), IND = 35, GER = 39, UK = 45, FRA = 48, AUS = 69 });
                data.Add(new Measurement() { Year = yr.AddYears(6), IND = 30, GER = 37, UK = 50, FRA = 46, AUS = 74 });
            }*/
        }

        public async Task PopulateGraphData(Sensor Sensor)
        {
            //What we're doing here is dirty
            //We take a random measurement from every so often,
            //then don't bother averaging out the other measurements,
            //and put the randomly chosen value in the graph
            const int PreferredMeasurementsAmount = 70;
            var MeasurementsList = await APITalker.Instance.getMeasurements(48, Sensor.ID);
            
            //Convert to new model using DateTime object, for time axis labeling purposes
            int step = MeasurementsList.Count / PreferredMeasurementsAmount;
            
            var DisplayedList = new ObservableCollection<Measurement>();
            for (int i = 0; i+step < MeasurementsList.Count; i += step)
            {
                Measurement average = new Measurement { MeasurementUnit = MeasurementsList[0].MeasurementUnit,
                                                        SensorID = Sensor.ID,
                                                        Timestamp = MeasurementsList[i+(step/2)].Timestamp,
                                                        Value = 0};
                for (int j = i; j < i+step; j++)
                {
                    average.Value += MeasurementsList[j].Value;
                }
                average.Value = average.Value / step;
                DisplayedList.Add(average);
            }
            System.Diagnostics.Debug.WriteLine("Load complete");
            this.data = DisplayedList;
            //this.data = new ObservableCollection<Measurement>(await APITalker.Instance.getMeasurements(24, 1));
        }

        private ObservableCollection<Measurement> _data = new ObservableCollection<Measurement>();
        public ObservableCollection<Measurement> data
        { get { return _data; } set { Set(ref _data, value); base.RaisePropertyChanged(); } }

    }
}

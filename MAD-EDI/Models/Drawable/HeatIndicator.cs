﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;
using Windows.Foundation;
using Windows.UI.Xaml.Media;

namespace MAD_EDI.Models.Drawable
{
    public class HeatIndicator : BindableBase
    {
        //Data properties
        private static float USAGE_MAXIMUM = 2500.0f; //The highest considerable power usage 
        private static float USAGE_MINIMUM = 0.0f;    //The lowest considerable power usage
        private float _Usage;
        //Drawing properties
        private int _Radius;
        private int _TextWidth; //Actually the diameter
        private Point _CenterPoint;
        private Point _NameTextPoint;
        private Point _UsageTextPoint;
        private Brush _Fill;
        private Brush _Stroke;

        public int Radius
        {
            get { return _Radius; }
            set { Set(ref _Radius, value); base.RaisePropertyChanged(); }
        }
        public int TextWidth
        {
            get { return _TextWidth; }
            set { Set(ref _TextWidth, value); base.RaisePropertyChanged(); }
        }
        public Brush Fill
        {
            get { return _Fill; }
            set { Set(ref _Fill, value); base.RaisePropertyChanged(); }
        }
        public Brush Stroke
        {
            get { return _Stroke; }
            set { Set(ref _Stroke, value); base.RaisePropertyChanged(); }
        }
        public Point CenterPoint
        {
            get { return _CenterPoint; }
            set { Set(ref _CenterPoint, value); base.RaisePropertyChanged(); }
        }
        public Point NameTextPoint
        {
            get { return _NameTextPoint; }
            set { Set(ref _NameTextPoint, value); base.RaisePropertyChanged(); }
        }
        public Point UsageTextPoint
        {
            get { return _UsageTextPoint; }
            set { Set(ref _UsageTextPoint, value); base.RaisePropertyChanged(); }
        }
        public string UsageText
        {
            get { return _Usage + "W" ; }
        }
        public float Usage
        {
            get { return _Usage; }
            set { Set(ref _Usage,value); Fill = new SolidColorBrush(DetermineHeatMapMultiColourGradient(value)); base.RaisePropertyChanged(); }
        }
        public String SensorName
        {
            get { return Sensor.Name; }
        }

        public Sensor Sensor { get; set; } //To be passed to a details page
    
        public HeatIndicator(int radius, double xPos, double yPos, float usage, Sensor sensor)
        {
            this.Sensor = sensor;
            this.Radius = radius; //The radius is currently passed as parameter, but should/could be calculated based on usage
            this.CenterPoint = new Point(xPos, yPos);
            this.UsageTextPoint = new Point(xPos - radius, yPos-10);       //Values chosen by trial & error
            this.NameTextPoint = new Point(xPos - radius, yPos-radius-18); 
            this.TextWidth = 2 * radius;
            this.Usage = usage;
            //Based on the radius colour the brush
            this.Fill = new SolidColorBrush(DetermineHeatMapMultiColourGradient(usage));
            this.Stroke = new SolidColorBrush(Windows.UI.Colors.Black); //Border 
        }

        //With the help of: http://www.andrewnoske.com/wiki/Code_-_heatmaps_and_color_gradients
        //Which is really, really awesome. Thanks Andrew!
        private static Windows.UI.Color DetermineHeatMapBiColourGradient(float value)
        {
            float normalisedValue = (value - USAGE_MINIMUM ) / (USAGE_MAXIMUM - USAGE_MINIMUM);

            int aR = 0; int aG = 0; int aB = 255;  // Colour to the extreme left
            int bR = 255; int bG = 0; int bB = 0;  // Colour to the extreme right
            byte r = (byte)((bR - aR) * normalisedValue + aR); // Evaluated as -255*value + 255.
            byte g = (byte)((bG - aG) * normalisedValue + aG); // Evaluates as 0.
            byte b = (byte)((bB - aB) * normalisedValue + aB); // Evaluates as 255*value + 0.
            return Windows.UI.Color.FromArgb(125, r, g, b); //Semi transparent
        }

        private static Windows.UI.Color DetermineHeatMapMultiColourGradient(float value)
        {
            float normalisedValue = (value - USAGE_MINIMUM) / (USAGE_MAXIMUM - USAGE_MINIMUM);

            const int extremeColoursCount = 5;
            float[,] extremeColours = new float[extremeColoursCount, 3] { { 0, 0, 255 }, { 0, 255, 0 }, { 255, 255, 0 }, { 255, 128, 0 }, { 255, 0, 0 } };
            // A static array of 4 colors:  (blue,   green,  yellow, orange,  red) using nromalised {r,g,b} values for each.

            int idx1;        // |-- Our desired color will be between these two indexes in "color".
            int idx2;        // |
            float fractBetween = 0;  // Fraction between "idx1" and "idx2" where our value is.

            if (normalisedValue <= 0) { idx1 = idx2 = 0; }    // accounts for an input <=0
            else if (normalisedValue >= 1) { idx1 = idx2 = extremeColoursCount - 1; }    // accounts for an input >=0
            else
            {
                normalisedValue = normalisedValue * (extremeColoursCount - 1);  // Will multiply value by 4.
                idx1 = (int)Math.Floor(normalisedValue);                        // Our desired color will be after this index.
                idx2 = idx1 + 1;                                      // ... and before this index (inclusive).
                fractBetween = normalisedValue - idx1;                          // Distance between the two indexes (0-1).
            }

            float r = ((extremeColours[idx2, 0] - extremeColours[idx1, 0]) * fractBetween + extremeColours[idx1, 0]);
            float g = ((extremeColours[idx2, 1] - extremeColours[idx1, 1]) * fractBetween + extremeColours[idx1, 1]);
            float b = ((extremeColours[idx2, 2] - extremeColours[idx1, 2]) * fractBetween + extremeColours[idx1, 2]);
            byte red = (byte)r;
            byte green = (byte)g;
            byte blue = (byte)b;
            return Windows.UI.Color.FromArgb(125, red, green, blue); //Semi transparent
        }
    }
}

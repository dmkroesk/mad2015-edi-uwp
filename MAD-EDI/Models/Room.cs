﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace MAD_EDI.Models
{
    public class Room
    {
        public int ID { get; set; }
        public String Name { get; set;  }
        public String MapPath { get; set; }
        public Int32 Width { get; set; }
        public Int32 Height { get; set; }
        public String Description { get; set; }
        public List<Sensor> sensors { get; set; }
    }
}

﻿using System;

namespace MAD_EDI.Models
{
    public class Measurement
    {
        public Int32 SensorID { get; set; }
        public DateTime Timestamp { get; set; }
        public String MeasurementUnit { get; set; }
        public float Value { get; set; }
    }
}

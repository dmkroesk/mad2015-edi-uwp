﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace MAD_EDI.Models
{
    public class Organistation
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public List<Building> Buildings { get; set; }

        public Organistation(int ID, String Name, String Description, List<Building> Buildings)
        {
            this.ID = ID;
            this.Name = Name;
            this.Description = Description;
            this.Buildings = new List<Building>();
            this.Buildings = Buildings;
        }

        public Organistation(String jsonString)
        {
            JsonObject jsonObject = JsonObject.Parse(jsonString);
            useJsonObject(jsonObject);

        }

        public Organistation(JsonObject jsonObject)
        {
            useJsonObject(jsonObject);
        }

        public Organistation()
        {
        }

        private void useJsonObject(JsonObject jsonObject)
        {
            this.Buildings = new List<Building>();
            ID = Convert.ToInt32(jsonObject.GetNamedNumber("ID", 0));
            Name = jsonObject.GetNamedString("Name", "");
            Description = jsonObject.GetNamedString("Description", "");
        }
    }
}

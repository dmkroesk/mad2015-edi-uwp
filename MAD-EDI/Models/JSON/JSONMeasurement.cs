﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAD_EDI.Models
{
    public class JSONMeasurement
    {
        public List<Measurement> measurements { get; set; }
        public List<Measurement> measurement { set; get; }

        public Measurement getMeasurement()
        {
            Measurement m = new Measurement();
            if (measurement != null)
            {
                if (measurement.Count == 1)
                {
                    return measurement[0];
                }
            }
            return m;
        }
    }
}

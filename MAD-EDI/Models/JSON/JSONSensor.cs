﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAD_EDI.Models
{
        public class JSONSensor{
            public List<Sensor> sensors { get; set; }
            public Sensor sensor { get; set; }
        }
    }

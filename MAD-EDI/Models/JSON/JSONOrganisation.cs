﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAD_EDI.Models.JSON
{
    class JSONOrganisation
    {

        public List<Organistation> organisations { get; set; }
        public List<Organistation> organisation { set; get; }

        public Organistation getOrganisation()
        {
            Organistation o = new Organistation();
            if (organisations != null)
            {
                if (organisations.Count == 1)
                {
                    return organisations[0];
                }
            }
            return o;
        }
    }
}

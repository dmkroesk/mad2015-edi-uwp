﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAD_EDI.Models.JSON
{
    class JSONBuilding
    {

        public List<Building> buildings { get; set; }
        public List<Building> building { set; get; }

        public Building getBuilding()
        {
            Building b = new Building();
            if (buildings != null)
            {
                if (buildings.Count == 1)
                {
                    return buildings[0];
                }
            }
            return b;
        }
    }
}

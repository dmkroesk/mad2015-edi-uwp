﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace MAD_EDI.Models
{
    public class Building
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public String Street { get; set; }
        public String StreetNumber { get; set; }
        public String City { get; set; }
        public String Description { get; set; }
        public List<Room> Rooms { get; set; }

        public Building(int ID, String Name, String Street, String City, String Description, List<Room> Rooms)
        {
            this.ID = ID;
            this.Name = Name;
            this.Street = Street;
            this.City = City;
            this.Description = Description;
            this.Rooms = new List<Room>();
            this.Rooms = Rooms;
        }

        public Building(JsonObject jsonObject)
        {
            useJsonObject(jsonObject);
        }
        
        public Building(String jsonString)
        {
            JsonObject jsonObject = JsonObject.Parse(jsonString);
            useJsonObject(jsonObject);
        }

        public Building()
        {
        }

        private void useJsonObject(JsonObject jsonObject)
        {
            this.Rooms = new List<Room>();
            ID = Convert.ToInt32(jsonObject.GetNamedNumber("ID", 0));
            Name = jsonObject.GetNamedString("Name", "");
            Street = jsonObject.GetNamedString("Street", "");
            StreetNumber = jsonObject.GetNamedString("StreetNumber", "");
            City = jsonObject.GetNamedString("City", "");
            Description = jsonObject.GetNamedString("Description", "");
        }
    }
}

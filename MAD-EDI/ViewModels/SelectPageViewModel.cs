﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Template10.Services.NavigationService;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Navigation;
using MAD_EDI.Mvvm;
using Windows.UI.Xaml.Media;
using System;
using System.Collections.ObjectModel;
using MAD_EDI.Models.Drawable;
using Windows.Foundation;
using MAD_EDI.Services;
using MAD_EDI.Models;
using Windows.UI.Xaml.Controls;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using MAD_EDI.Services.SettingsServices;

namespace MAD_EDI.ViewModels
{
    public class SelectPageViewModel : ViewModelBase
    {
        private ObservableCollection<Organistation> _Organistations = new ObservableCollection<Organistation>();
        public ObservableCollection<Organistation> Organistations { get { return _Organistations; } set { Set(ref _Organistations, value); base.RaisePropertyChanged(); } }
        private ObservableCollection<Building> _Buildings = new ObservableCollection<Building>();
        public ObservableCollection<Building> Buildings { get { return _Buildings; } set { Set(ref _Buildings, value); base.RaisePropertyChanged(); } }
        public Organistation Organisation
        {
            get { return SettingsService.Instance.OrganisationSelected; }
            set
            {
                if (SettingsService.Instance.OrganisationSelected != value)
                {
                    SettingsService.Instance.OrganisationSelected = value;
                    base.RaisePropertyChanged();
                }
            }
        }
        public Building Building
        {
            get { return SettingsService.Instance.BuildingSelected; }
            set
            {
                if (SettingsService.Instance.BuildingSelected != value)
                {
                    SettingsService.Instance.BuildingSelected = value;
                    base.RaisePropertyChanged();
                }
            }
        }

        public SelectPageViewModel()
        {
            var o = loadOrganisations();
        }


        public async Task loadOrganisations()
        {
            List<Organistation> organisations = await APITalker.GetInstance().getOrganisations();

            foreach (Organistation o in organisations)
            {
                Organistations.Add(o);
                //await loadBuildingsByOrganisation(o);
            }
        }

        public void setOrganisation_changed(object sender, RoutedEventArgs e)
        {
            var item = ((sender as ListBox).SelectedItem as Organistation);
            var building = loadBuildingsByOrganisation(item);
            Organisation = item;
        }

        public void setBuilding_changed(object sender, RoutedEventArgs e)
        {
            SettingsService.Instance.BuildingSelected = ((sender as ListBox).SelectedItem as Building);
            Views.Shell shell = (Views.Shell)Window.Current.Content;
            var task = shell.loadRoomsAsyncByBuilding(SettingsService.Instance.BuildingSelected);
        }

        public async Task loadBuildingsByOrganisation(Organistation organisation)
        {
            List<Building> buildings = await APITalker.GetInstance().getBuildingsByOrganisation(organisation);
            Buildings = new ObservableCollection<Building>();
            foreach (Building b in buildings)
            {
                Buildings.Add(b);
            }
        }
        public override void OnNavigatedTo(object parameter, NavigationMode mode, IDictionary<string, object> state)
        {
            if (state.Any())
            {
                // use cache value(s)
                if (state.ContainsKey(nameof(Value))) Value = state[nameof(Value)]?.ToString();
                // clear any cache
                state.Clear();
            }
            else
            {
                // use navigation parameter
                Value = parameter?.ToString();
            }

        }

        public override Task OnNavigatedFromAsync(IDictionary<string, object> state, bool suspending)
        {
            if (suspending)
            {
                // persist into cache
                state[nameof(Value)] = Value;
            }
            return base.OnNavigatedFromAsync(state, suspending);
        }

        public override void OnNavigatingFrom(NavigatingEventArgs args)
        {
            base.OnNavigatingFrom(args);
        }

        private string _Value = "Default";
        public string Value
        {
            get { return _Value; }
            set
            {
                Set(ref _Value, value);
            }
        }

    }
}


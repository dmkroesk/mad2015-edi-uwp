using MAD_EDI.Models;
using MAD_EDI.Models.Drawable;
using MAD_EDI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;
using Template10.Services.NavigationService;
using Windows.UI.Xaml.Navigation;

namespace MAD_EDI.ViewModels
{
    public class DetailPageViewModel : MAD_EDI.Mvvm.ViewModelBase
    {
        //Practically the constructor. This page should only be navigated to with a sensor
        public override void OnNavigatedTo(object parameter, NavigationMode mode, IDictionary<string, object> state)
        {
            if (state.Any())
            {
                // use cache value(s) (Requires deserialising Sensor)

                //if (state.ContainsKey(nameof(Sensor))) Sensor = state[nameof(Sensor)]?.ToString();
                // clear any cache
                //state.Clear();
            }
            else
            {
                // use navigation parameter(s) to set binded relevant information.
                Sensor = parameter as Sensor; 
                this.GraphLineModel = new GraphLineModel(Sensor);
            }
        }

        public override Task OnNavigatedFromAsync(IDictionary<string, object> state, bool suspending)
        {
            if (suspending)
            {
                // persist into cache (Requires serialising Sensor)

                //state[nameof(Sensor)] = Sensor;
            }
            return base.OnNavigatedFromAsync(state, suspending);
        }

        public override void OnNavigatingFrom(NavigatingEventArgs args)
        {
            args.Cancel = false;
        }

        private GraphLineModel _GraphLineModel;
        public GraphLineModel GraphLineModel { get { return _GraphLineModel;  } set { Set(ref _GraphLineModel, value); } }

        private Sensor _Sensor;
        public Sensor Sensor { get { return _Sensor; } set { Set(ref _Sensor, value); } }

        private Measurement _LastMeasurement;
        public Measurement LastMeasurement { get { return _LastMeasurement; } set { Set(ref _LastMeasurement, value); } }
    }
}


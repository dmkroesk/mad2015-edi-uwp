using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Template10.Services.NavigationService;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Navigation;
using MAD_EDI.Mvvm;
using Windows.UI.Xaml.Media;
using System;
using System.Collections.ObjectModel;
using MAD_EDI.Models.Drawable;
using Windows.Foundation;
using MAD_EDI.Services;
using MAD_EDI.Models;
using Windows.UI.Xaml.Controls;
using Syncfusion.UI.Xaml.Charts;
using Windows.UI.Popups;
using Windows.UI.Xaml.Data;
using System.ComponentModel;

namespace MAD_EDI.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public FloorplanPartViewModel FloorplanPartViewModel { get; } = new FloorplanPartViewModel();
        public TimeLapsePartViewModel TimeLapsePartViewModel { get; } = new TimeLapsePartViewModel();
        public string _PageTitle = "Room page / Kamer pagina";
        public string PageTitle { get { return _PageTitle; } set {
                Set(ref _PageTitle, value);
                base.RaisePropertyChanged();
            } }
        public MainPageViewModel()
        {
            var test = TestAPIConnection();
        }
        public override void OnNavigatedTo(object parameter, NavigationMode mode, IDictionary<string, object> state)
        {
            if (state.Any())
            {
                // use cache value(s)
                if (state.ContainsKey(nameof(Value))) Value = state[nameof(Value)]?.ToString();
                // clear any cache
                state.Clear();
            }
            else
            {
                // use navigation parameter
                Value = parameter?.ToString();
            }
        }

        public override Task OnNavigatedFromAsync(IDictionary<string, object> state, bool suspending)
        {
            if (suspending)
            {
                // persist into cache
                state[nameof(Value)] = Value;
            }
            return base.OnNavigatedFromAsync(state, suspending);
        }

        public override void OnNavigatingFrom(NavigatingEventArgs args)
        {
            base.OnNavigatingFrom(args);
        }

        private string _Value = "Default";
        public string Value {
            get { return _Value; }
            set {
                int roomID;
                if (value != null && value != "Default" && int.TryParse(value, out roomID))
                {
                    var task = FloorplanPartViewModel.loadFloorplanDataAsync(roomID, this);
                    var loadTimelapse = TimeLapsePartViewModel.loadTimelapseDataAsync(roomID);
                }
                Set(ref _Value, value);
            }
        }
        private void CommandInvokedHandler(IUICommand command)
        {
            NavigationService.Navigate(typeof(Views.SettingsPage), 0);
        }
        public async Task TestAPIConnection()
        {
            if (!await APITalker.GetInstance().TestConnection())
            {
                var messageDialog = new MessageDialog("Verbinding is mislukt");
                messageDialog.Commands.Add(new UICommand("Ga naar settings",
                    new UICommandInvokedHandler(this.CommandInvokedHandler)));

                // Set the command to be invoked when escape is pressed
                messageDialog.CancelCommandIndex = 1;

                // Show the message dialog
                await messageDialog.ShowAsync();
            }
            
        }

        public void Heatindicator_Tap(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            var item = ((sender as Canvas).DataContext as HeatIndicator);
            if (item != null && item.Sensor != null)
            {
                NavigationService.Navigate(typeof(Views.DetailPage), item.Sensor);
            }
        }

    }

    public class FloorplanPartViewModel : ViewModelBase
    {
        //Image testing properties
        public static Uri imgUri;
        private Room room { get; set; }
        //Databound image objects
        private ImageSource _FloorplanSource = null;
        public ImageSource FloorplanSource { get { return _FloorplanSource; } set { Set(ref _FloorplanSource, value); base.RaisePropertyChanged(); } }
        private Image _FloorplanImage = new Image();
        public Image FloorplanImage
        { get { return _FloorplanImage; } set { Set(ref _FloorplanImage, value); base.RaisePropertyChanged(); } }

        //Canvas items      
        private ObservableCollection<HeatIndicator> _HeatIndicatorItems = new ObservableCollection<HeatIndicator>();
        public ObservableCollection<HeatIndicator> HeatIndicatorItems
        { get { return _HeatIndicatorItems; } set { Set(ref _HeatIndicatorItems, value); base.RaisePropertyChanged(); } }

        public FloorplanPartViewModel()
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
                // designtime data example of loading resource text codewise
                var resourceLoader = ResourceLoader.GetForCurrentView();
                return;
            }
        }

        public async Task loadFloorplanDataAsync(int roomID)
        {
            await loadFloorplanDataAsync(roomID, null);
        }
        public async Task loadFloorplanDataAsync(int roomID, MainPageViewModel mpv)
        {
            //Test HeatIndicators
            APITalker apiTalker = APITalker.GetInstance();
            Room room = await APITalker.GetInstance().getRoomByID(roomID);
            if (mpv != null)
            {
                mpv.PageTitle = room.Name;
            }
            SetFloorplanSource(room.MapPath);
            List<Sensor> sensors = await apiTalker.getSensorsByRoom(room.ID);

            foreach (Sensor s in sensors)
            {
                Measurement m = await apiTalker.getLastMeasurement(s.ID);
                HeatIndicatorItems.Add(new HeatIndicator(50,
                                                        75+s.MapLocationX*85,
                                                        25+s.MapLocationY*85,
                                                        m.Value,
                                                        s));
            }
        }

        public void SetFloorplanSource(string uriPath)
        {
            //Should do a proper try catch for creating a valid URI
            if (uriPath.Length > 0)
            {
                Uri newUri = new Uri(uriPath);
                FloorplanSource = new Windows.UI.Xaml.Media.Imaging.BitmapImage(newUri);
            }
        }
        
    }

    public class TimeLapsePartViewModel : ViewModelBase
    {
        private ObservableCollection<HeatIndicator> _HeatIndicatorItems = new ObservableCollection<HeatIndicator>();
        public ObservableCollection<HeatIndicator> HeatIndicatorItems
        { get { return _HeatIndicatorItems; } set { Set(ref _HeatIndicatorItems, value); base.RaisePropertyChanged(); } }

        private int indexPlay = 0;
        private String _Time = "";
        public String Time
        { get { return _Time; } set { Set(ref _Time, value); base.RaisePropertyChanged(); } }

        Dictionary<Sensor, List<Measurement>> heatmapData = new Dictionary<Sensor, List<Measurement>>();
        private BackgroundWorker bw = new BackgroundWorker();
        //Timelapse things
        public TimeLapsePartViewModel()
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
                // designtime data example of loading resource text codewise
                var resourceLoader = ResourceLoader.GetForCurrentView();
                return;
            }

        }
        public void StopBtn_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            bw.CancelAsync();
        }
        public void PlayBtn_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
        

            // this allows our worker to report progress during work
            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;

            // what to do in the background thread
            bw.DoWork += new DoWorkEventHandler(
            delegate (object o, DoWorkEventArgs args)
            {
                BackgroundWorker b = o as BackgroundWorker;
                for (int i = indexPlay; i < 17280; i++)
                {
                    if (bw.CancellationPending)
                    {
                        indexPlay = i;
                        args.Cancel = true;
                        return;
                    }
                    b.ReportProgress(i);
                    Task.Delay(250).Wait();
                }
                indexPlay = 0;

            });

            // what to do when progress changed (update the progress bar for example)
            bw.ProgressChanged += new ProgressChangedEventHandler(
            delegate (object o, ProgressChangedEventArgs args)
            {
                HeatIndicatorItems.Clear();
                int i = args.ProgressPercentage;
                Measurement timeMeasurement = new Measurement();
                foreach (KeyValuePair<Sensor, List<Measurement>> pair in heatmapData)
                {
                    if (i < pair.Value.Count)
                    {
                        refreshFloorplan(pair.Key, pair.Value[i]);
                        timeMeasurement = pair.Value[i];
                    }
                }
                Time = timeMeasurement.Timestamp.ToString("dd-MM-yyyy hh:mm:ss");
                base.RaisePropertyChanged();
            });

            // what to do when worker completes its task (notify the user)
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(
            delegate (object o, RunWorkerCompletedEventArgs args)
            {
                System.Diagnostics.Debug.WriteLine("Finished!");
            });

            bw.RunWorkerAsync();
        }

        public async Task loadTimelapseDataAsync(int roomID)
        {
            //Test HeatIndicators
            APITalker apiTalker = APITalker.GetInstance();
            List<Sensor> sensors = await apiTalker.getSensorsByRoom(roomID);

            foreach (Sensor s in sensors)
            {
                List<Measurement> measurements = await apiTalker.getMeasurements(24, s.ID);
                heatmapData.Add(s, measurements);
            }
        }

        public void refreshFloorplan(Sensor s, Measurement m)
        {
            HeatIndicatorItems.Add(new HeatIndicator(50,
                                                    75 + s.MapLocationX * 85,
                                                    25 + s.MapLocationY * 85,
                                                    m.Value,
                                                    s));
        }
    }

    public class LineChartLabelConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return String.Format("{0}", value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value;
        }
        #endregion
    }
}


﻿using MAD_EDI.Models;
using MAD_EDI.Models.JSON;
using RestSharp.Portable;
using RestSharp.Portable.Deserializers;
using RestSharp.Portable.HttpClient;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace MAD_EDI.Services
{
    public sealed class APITalker
    {
        public Token token { set; get; }
        public String apiURL { set; get; }
        private RestClient client { set; get; }
        SettingsServices.SettingsService _settings;
        private APITalker() {
            _settings = SettingsServices.SettingsService.Instance;
            setRestClient();
        }

        public void setRestClient()
        {
            apiURL = _settings.ServerURL;
            client = new RestClient(apiURL);
        }

        public async Task<Token> getToken()
        {
            var request = new RestRequest("api/login", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(new { username = _settings.ServerUsername, password = _settings.ServerPassword });

            try
            {
                var respons = await client.Execute(request);
                if (respons != null)
                {
                    JsonDeserializer deserial = new JsonDeserializer();
                    token = deserial.Deserialize<Token>(respons);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return token;
        }

        public async Task<Boolean> TestConnection()
        {
            setRestClient();
            var request = new RestRequest("api/login", Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(new { username = _settings.ServerUsername, password = _settings.ServerPassword });

            try
            {
                var respons = await client.Execute(request);
                if (respons != null)
                {
                    JsonDeserializer deserial = new JsonDeserializer();
                    Token t = deserial.Deserialize<Token>(respons);
                    if (t.token != "")
                    {
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return false;
        }

        public async Task<List<Measurement>> getMeasurements(int sensor)
        {
            return await getMeasurements(24, sensor);
        }


        public async Task<Measurement> getLastMeasurement(int sensor)
        {
            JSONMeasurement jsonMeasurements = new JSONMeasurement();
            if (token == null)
            {
                return jsonMeasurements.getMeasurement();
            }

            String url = "apiv2/sensors/" + sensor + "/lastmeasurement";
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Access-Token", token.token);

            try
            {
                var respons = await client.Execute(request);
                if (respons != null)
                {
                    JsonDeserializer deserial = new JsonDeserializer();
                    jsonMeasurements = deserial.Deserialize<JSONMeasurement>(respons);
                    System.Diagnostics.Debug.WriteLine(jsonMeasurements);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }

            return jsonMeasurements.getMeasurement();
        }

        public async Task<List<Measurement>> getMeasurements(double hours, int sensor)
        {
            JSONMeasurement jsonMeasurements = new JSONMeasurement();
            if (token == null)
            {
                return jsonMeasurements.measurements;
            }
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            String url = "apiv2/sensors/"+sensor+"/measurements/" + hours.ToString(nfi);
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Access-Token", token.token);

            try
            {
                var respons = await client.Execute(request);
                if (respons != null)
                {
                    JsonDeserializer deserial = new JsonDeserializer();
                    jsonMeasurements = deserial.Deserialize<JSONMeasurement>(respons);
                    System.Diagnostics.Debug.WriteLine(jsonMeasurements);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return jsonMeasurements.measurements;
        }

        public async Task<Room> getRoomByID(int ID)
        {
            JSONRoom jsonRoom = new JSONRoom();
            if (token == null)
            {
                return jsonRoom.getRoom();
            }
            String url = "api/rooms/" + ID;
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Access-Token", token.token);

            try
            {
                var respons = await client.Execute(request);
                if (respons != null)
                {
                    JsonDeserializer deserial = new JsonDeserializer();
                    jsonRoom = deserial.Deserialize<JSONRoom>(respons);
                    System.Diagnostics.Debug.WriteLine(jsonRoom);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return jsonRoom.getRoom();
        }

        public async Task<List<Organistation>> getOrganisations()
        {
            JSONOrganisation jsonOrganisation = new JSONOrganisation();
            if (token == null)
            {
                return jsonOrganisation.organisations;
            }
            String url = "api/organisations/";
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Access-Token", token.token);

            try
            {
                var respons = await client.Execute(request);
                if (respons != null)
                {
                    JsonDeserializer deserial = new JsonDeserializer();
                    jsonOrganisation = deserial.Deserialize<JSONOrganisation>(respons);
                    System.Diagnostics.Debug.WriteLine(jsonOrganisation);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return jsonOrganisation.organisations;
        }

        public async Task<List<Building>> getBuildingsByOrganisation(Organistation o)
        {
            JSONBuilding jsonBuilding = new JSONBuilding();
            if (token == null)
            {
                return jsonBuilding.buildings;
            }
            String url = "api/organisations/"+o.ID+"/buildings";
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Access-Token", token.token);

            try
            {
                var respons = await client.Execute(request);
                if (respons != null)
                {
                    JsonDeserializer deserial = new JsonDeserializer();
                    jsonBuilding = deserial.Deserialize<JSONBuilding>(respons);
                    System.Diagnostics.Debug.WriteLine(jsonBuilding);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return jsonBuilding.buildings;
        }

        public async Task<List<Room>> getRoomsByBuilding(int building)
        {
            JSONRoom jsonRoom = new JSONRoom();
            if (token == null)
            {
                return jsonRoom.rooms;
            }
            String url = "api/buildings/" + building + "/rooms";
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Access-Token", token.token);

            try
            {
                var respons = await client.Execute(request);
                if (respons != null)
                {
                    JsonDeserializer deserial = new JsonDeserializer();
                    jsonRoom = deserial.Deserialize<JSONRoom>(respons);
                    System.Diagnostics.Debug.WriteLine(jsonRoom);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return jsonRoom.rooms;
        }

        public async Task<List<Sensor>> getSensorsByRoom(int room)
        {
            JSONSensor jsonSensor = new JSONSensor();
            if (token == null)
            {
                return jsonSensor.sensors;
            }
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            String url = "api/rooms/" + room + "/sensors";
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("X-Access-Token", token.token);

            try
            {
                var respons = await client.Execute(request);
                if (respons != null)
                {
                    JsonDeserializer deserial = new JsonDeserializer();
                    jsonSensor = deserial.Deserialize<JSONSensor>(respons);
                    System.Diagnostics.Debug.WriteLine(jsonSensor);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            return jsonSensor.sensors;
        }

        // this will be initialized only once
        private static APITalker instance = new APITalker();

        // Do you prefer a Property?
        public static APITalker Instance
        {
            get
            {
                return instance;
            }
        }

        // or, a Method?
        public static APITalker GetInstance()
        {
            return instance;
        }
    }
}

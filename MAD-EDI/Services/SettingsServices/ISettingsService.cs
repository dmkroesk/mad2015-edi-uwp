using MAD_EDI.Models;
using System;
using Windows.UI.Xaml;

namespace MAD_EDI.Services.SettingsServices
{
    public interface ISettingsService
    {
        ApplicationTheme AppTheme { get; set; }
        TimeSpan CacheMaxDuration { get; set; }
        String ServerURL { get; set; }
        String ServerUsername { get; set; }
        String ServerPassword { get; set; }
        Building BuildingSelected { get; set; }
        Organistation OrganisationSelected { get; set; }

    }
}

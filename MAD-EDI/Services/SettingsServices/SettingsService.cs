using MAD_EDI.Models;
using System;
using Windows.UI.Xaml;

namespace MAD_EDI.Services.SettingsServices
{
    // DOCS: https://github.com/Windows-XAML/Template10/wiki/Docs-%7C-SettingsService
    public partial class SettingsService : ISettingsService
    {
        public static SettingsService Instance { get; }
        static SettingsService()
        {
            // implement singleton pattern
            Instance = Instance ?? new SettingsService();
        }

        Template10.Services.SettingsService.ISettingsHelper _helper;
        private SettingsService()
        {
            _helper = new Template10.Services.SettingsService.SettingsHelper();
        }

        public ApplicationTheme AppTheme
        {
            get
            {
                var theme = ApplicationTheme.Light;
                var value = theme.ToString();
                return Enum.TryParse<ApplicationTheme>(value, out theme) ? theme : ApplicationTheme.Light;
            }
            set
            {
                _helper.Write(nameof(AppTheme), value.ToString());
                ApplyAppTheme(value);
            }
        }

        public TimeSpan CacheMaxDuration
        {
            get { return _helper.Read<TimeSpan>(nameof(CacheMaxDuration), TimeSpan.FromDays(2)); }
            set
            {
                _helper.Write(nameof(CacheMaxDuration), value);
                ApplyCacheMaxDuration(value);
            }
        }

        public string ServerURL
        {
            get
            {
                var host = "http://145.48.6.95:8080/";
                var value = _helper.Read<string>(nameof(ServerURL), host.ToString());
                return value;
            }
            set
            {
                _helper.Write(nameof(ServerURL), value.ToString());
            }
        }

        public string ServerUsername
        {
            get
            {
                var username = "ediuser";
                var value = _helper.Read<string>(nameof(ServerUsername), username.ToString());
                return value;
            }
            set
            {
                _helper.Write(nameof(ServerUsername), value.ToString());
            }
        }

        public string ServerPassword
        {
            get
            {
                var password = "edipassword";
                var value = _helper.Read<string>(nameof(ServerPassword), password.ToString());
                return value;
            }
            set
            {
                _helper.Write(nameof(ServerPassword), value.ToString());
            }
        }

        public Organistation OrganisationSelected
        {
            get
            {
                var value = _helper.Read<Organistation>(nameof(OrganisationSelected), null);
                return value;
            }
            set
            {
                _helper.Write(nameof(OrganisationSelected), value);
            }
        }

        public Building BuildingSelected
        {
            get
            {
                var value = _helper.Read<Building>(nameof(BuildingSelected), null);
                return value;
            }
            set
            {
                _helper.Write(nameof(BuildingSelected), value);
            }
        }
    }
}


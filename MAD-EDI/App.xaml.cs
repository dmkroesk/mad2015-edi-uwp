using System;
using Windows.UI.Xaml;
using System.Threading.Tasks;
using MAD_EDI.Services.SettingsServices;
using Windows.ApplicationModel.Activation;
using MAD_EDI.Models;
using System.Collections.Generic;
using MAD_EDI.Services;
using RestSharp.Portable.HttpClient;
using RestSharp.Portable;
using RestSharp.Portable.Authenticators;
using RestSharp.Portable.Deserializers;

namespace MAD_EDI
{
    /// Documentation on APIs used in this page:
    /// https://github.com/Windows-XAML/Template10/wiki

    sealed partial class App : Template10.Common.BootStrapper
    {
        ISettingsService _settings;

        public App()
        {
            InitializeComponent();
            SplashFactory = (e) => new Views.Splash(e);

            _settings = SettingsService.Instance;
            RequestedTheme = _settings.AppTheme;
            CacheMaxDuration = _settings.CacheMaxDuration;
            ShowShellBackButton = false;
        }

        // runs even if restored from state
        public override Task OnInitializeAsync(IActivatedEventArgs args)
        {
            var nav = NavigationServiceFactory(BackButton.Attach, ExistingContent.Include);
            Window.Current.Content = new Views.Shell(nav);
            return Task.FromResult<object>(null);
        }

        // runs only when not restored from state
        public override async Task OnStartAsync(StartKind startKind, IActivatedEventArgs args)
        {
            await Task.Delay(50);
            if(await APITalker.GetInstance().getToken() == null)
            {
                //Token is not set!
                // Offline modus?
            }

            //Loading the rooms in
            Views.Shell shell = (Views.Shell)Window.Current.Content;
            var task = shell.loadRoomsAsyncByBuilding(SettingsService.Instance.BuildingSelected);
            NavigationService.Navigate(typeof(Views.SelectPage));
        }
    }
}


using MAD_EDI.ViewModels;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;

namespace MAD_EDI.Views
{
    public sealed partial class SelectPage : Page
    {
        public SelectPage()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Disabled;
        }

        // strongly-typed view models enable x:bind
        public SelectPageViewModel ViewModel => this.DataContext as SelectPageViewModel;

        private void loadBuilding(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(sender.ToString());
        }
    }
}

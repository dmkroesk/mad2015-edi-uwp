using System;
using System.ComponentModel;
using System.Linq;
using Template10.Common;
using Template10.Controls;
using Template10.Services.NavigationService;
using Template10.Utils;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MAD_EDI.Services.SettingsServices;
using System.Collections.Generic;
using Windows.UI.Xaml.Media;
using MAD_EDI.Models;
using System.Collections;
using MAD_EDI.Services;
using System.Threading.Tasks;

namespace MAD_EDI.Views
{
    // DOCS: https://github.com/Windows-XAML/Template10/wiki/Docs-%7C-SplitView
    public sealed partial class Shell : Page
    {
        public static Shell Instance { get; set; }
        public static HamburgerMenu HamburgerMenu { get { return Instance.MyHamburgerMenu; } }
        private System.Collections.ObjectModel.ObservableCollection<HamburgerButtonInfo> designedHambugerMenu;

        public Shell(NavigationService navigationService)
        {

            Instance = this;
            InitializeComponent();
            if (navigationService != null) {
                MyHamburgerMenu.NavigationService = navigationService;
            }
            
            VisualStateManager.GoToState(Instance, Instance.NormalVisualState.Name, true);
        }
        public async Task loadRoomsAsyncByBuilding(Building building)
        {
            if (building != null)
            {
                List<Room> rooms = await APITalker.GetInstance().getRoomsByBuilding(building.ID);
                updateRoom(rooms);
            }
            
        }
        public void updateRoom(List<Room> rooms)
        {
            if (rooms != null)
            {
                while (MyHamburgerMenu.PrimaryButtons.Count > 1)
                {
                    MyHamburgerMenu.PrimaryButtons.RemoveAt(1);
                }
                foreach (Room room in rooms)
                {
                    //Symbool
                    SymbolIcon si = new SymbolIcon();
                    si.Width = 48;
                    si.Height = 48;
                    si.Symbol = Symbol.MapPin;

                    //TextBlock
                    TextBlock tb = new TextBlock();
                    tb.Margin = new Thickness(12, 0, 0, 0);
                    tb.VerticalAlignment = VerticalAlignment.Center;
                    tb.Text = room.Name;

                    //Stackpanel
                    StackPanel sp = new StackPanel();
                    sp.Orientation = Orientation.Horizontal;
                    sp.Children.Add(si);
                    sp.Children.Add(tb);

                    //HambugerButton
                    HamburgerButtonInfo hbi = new HamburgerButtonInfo();
                    hbi.PageType = typeof(MainPage);
                    hbi.PageParameter = room.ID;
                    hbi.ClearHistory = true;
                    hbi.Content = sp;
                    MyHamburgerMenu.PrimaryButtons.Add(hbi);

                }
            }
        }

        public static void SetBusyVisibility(Visibility visible, string text = null)
        {
            WindowWrapper.Current().Dispatcher.Dispatch(() =>
            {
                switch (visible)
                {
                    case Visibility.Visible:
                        Instance.FindName(nameof(BusyScreen));
                        Instance.BusyText.Text = text ?? string.Empty;
                        if (VisualStateManager.GoToState(Instance, Instance.BusyVisualState.Name, true))
                        {
                            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                                AppViewBackButtonVisibility.Collapsed;
                        }
                        break;
                    case Visibility.Collapsed:
                        if (VisualStateManager.GoToState(Instance, Instance.NormalVisualState.Name, true))
                        {
                            BootStrapper.Current.UpdateShellBackButton();
                        }
                        break;
                }
            });
        }
    }
}

